#!/bin/env python
import argparse
from PIL import Image


def saveImage(filenames, outfile, duration):
    if len(filenames) < 1:
        return

    imgs = []
    for filename in filenames:
        img = Image.open(filename)
        imgs.append(img)
    imgs[0].save(outfile, save_all=True, append_images=imgs[1:], duration=duration, loop=0)

parser = argparse.ArgumentParser(description='Generate a gif')
parser.add_argument('--verbose', default=False, action='store_const', const=True, help='Verbose mode')
parser.add_argument('filenameList', nargs='+', default=[], help='The list of files to add to the gif')
parser.add_argument('--out', default='out.gif', help='The output file.')
parser.add_argument('--duration', type=int, default=200, help='number of milliseconds duration to show each frame')
args = parser.parse_args()

v=args.verbose

saveImage(args.filenameList, args.out, args.duration)
